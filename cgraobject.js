class CGRAobject{
    vertexbuffer = -1;
    colorbuffer;
    shaderprog = -1;
    gl;
    objname = "NoName"
     constructor(glcontext){
         this.gl = glcontext;
         this.projMat = glm.mat4(1.0); // identity transformation by default
         this.viewMat = glm.mat4(1.0); // identity transformation by default
         this.modelMat = glm.mat4(1.0); // identity transformation by default
     }
    
    setShader(shader){
        this.shaderprog = shader;
    }
    
    setModelTransformation(modelmat4){
        this.modelMat = modelmat4;
    }
    
    drawit(viewMat, projectionMat){
        if (this.shaderprog == -1){
             alert('No shader assigned to object ' + this.name+ '. It will not be drawn!');
            throw new Error("No shader!")
        }
        if (this.vertexbuffer == -1){
             alert('No VBO assigned to object ' + this.name + '. It will not be drawn!');
             throw new Error("No VBO!")
        }
        
        this.shaderprog.startUsing();
        var mvploc = this.gl.getUniformLocation(this.shaderprog.shaderProgram,"MVP");
        
        var MVP = this.projMat['*'](this.viewMat);
        MVP = MVP['*'](this.modelMat);
        this.gl.uniformMatrix4fv(mvploc,false, MVP.array);
        
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER,this.vertexbuffer);
       
        this.positionLocation = this.gl.getAttribLocation(this.shaderprog.shaderProgram,
                                                          "in_Position");
        this.gl.vertexAttribPointer(this.positionLocation, // Attribute location
                           3, // number of elements per attribute
                           this.gl.FLOAT,  // Type of elements
                           this.gl.FALSE,  // 
                           3*Float32Array.BYTES_PER_ELEMENT, // size of a vertex in bytes 
                           0); // Offset from the begining of a single vertex to this attribute

        this.gl.enableVertexAttribArray(this.positionLocation);

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER,this.colorbuffer);
        this.colorLocation = this.gl.getAttribLocation(this.shaderprog.shaderProgram,
                                                          "in_Color");
        this.gl.vertexAttribPointer(this.colorLocation, // Attribute location
                           3, // number of elements per attribute
                           this.gl.FLOAT,  // Type of elements
                           this.gl.FALSE,  // 
                           3*Float32Array.BYTES_PER_ELEMENT, // size of a vertex in bytes 
                           0); // Offset from the begining of a single vertex to this attribute

        this.gl.enableVertexAttribArray(this.colorLocation);
       
        this.gl.drawArrays(this.gl.TRIANGLES,0,this.numvertices);
        this.shaderprog.stopUsing();
        
    }
};